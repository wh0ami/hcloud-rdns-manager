"""
The hcloud-rdns-manager - the smart way to manage your rDNS records in the Hetzner cloud.

 Author: wh0ami
License: MIT License <https://opensource.org/license/MIT>
Project: https://codeberg.org/wh0ami/hcloud-rdns-manager
"""
